package daemon

import (
	"gitlab.com/eyjhb/goating/module/note"
	"gitlab.com/eyjhb/goating/web"
)

func Start() error {
	// init web
	w, err := web.Init()
	if err != nil {
		return err
	}

	// load modules
	nm, err := note.Init(w)
	nm.Start()

	// start goroutine for modules crontab

	// start web service
	if err := w.Start(); err != nil {
		return err
	}

	return nil
}
