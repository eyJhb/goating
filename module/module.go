package module

type Module interface {
	Start() error
	Crontab() error
}
