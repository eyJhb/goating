package note

import (
	"net/http"

	"gitlab.com/eyjhb/goating/web"
)

const (
	Prefix = "notes"
)

type Note struct {
	w *web.Web
}

func Init(w *web.Web) (Note, error) {
	return Note{
		w: w,
	}, nil
}

func (n *Note) Start() error {
	// add routes
	n.w.AddRoute(Prefix, "", n.webAddNote, "GET")

	return nil
}

func (n *Note) Crontab() error {
	return nil
}

func (n *Note) webAddNote(w http.ResponseWriter, r *http.Request) {
}
