package web

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

const (
	ServerPort = "8080"
)

type Web struct {
	router *mux.Router
}

func Init() (*Web, error) {
	return &Web{
		router: mux.NewRouter().StrictSlash(true),
	}, nil
}

func (w *Web) AddRoute(prefix, url string, f func(w http.ResponseWriter, r *http.Request), methods ...string) error {
	routeUrl := fmt.Sprintf("/%s/%s", prefix, url)

	log.Infof("Adding route %s", routeUrl)

	w.router.HandleFunc(routeUrl, f).Methods(methods...)

	return nil
}

func (w *Web) Start() error {
	// base routes
	w.router.HandleFunc("/", mainIndex).Methods("GET")

	// serve our server!
	log.Infof("serving http server on port %s", ServerPort)
	if err := http.ListenAndServe(":"+ServerPort, w.router); err != nil {
		log.Fatalf("Failed to start http server, %v", err)
		return err
	}

	return nil
}

// func mainIndex(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
func mainIndex(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello world!")
	return
}
