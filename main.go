package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/eyjhb/goating/daemon"
)

func main() {
	fmt.Println("This is our main function!")

	if err := daemon.Start(); err != nil {
		log.Errorf("Failed to start daemon")
		return
	}
}
